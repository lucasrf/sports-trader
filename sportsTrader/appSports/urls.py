from django.contrib import admin
from django.urls import path, include
from . import views
urlpatterns = [

    path('', views.home, name='home'),
    path('login/',views.loginView,  name='login'),
    path('login/submit',views.login_submit,  name='submit'),
    path('logado/',views.index,  name='index'),
    path('logout/',views.logout_user,  name='logout'),
    path('register/',views.register_apostador,  name='register'),
    path('register/submit',views.set_apostador,  name='setapostador'),
    path("salas/", views.salas, name= "salas"),
    path("apostas/", views.apostas, name= "apostas"),
    path('salas/criarSala/', views.criarSala, name="criarSala"),
    path('salas/criarSala/submit', views.cadastrarSala, name="addSala"), 
    path('salas/verSalas/', views.verSala, name="verSala"),
    path('logado/submit',views.indexSubmit,  name='index'),
    path("apostas/submit", views.apostasSubmit, name= "apostassubmit"),
    path('showApostas/', views.showApostas, name='showAposta'),
    path('showApostas/submit', views.showApostasSubmit, name='showApostaSubmit'),
    path('resultados/', views.resultados,name='resultados'),
    path('resultados/submit',views.resultadosSubmit, name='resultadosSubmit')

]