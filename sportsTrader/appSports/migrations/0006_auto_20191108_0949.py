# Generated by Django 2.2.6 on 2019-11-08 12:49

from django.db import migrations, models
import uuid


class Migration(migrations.Migration):

    dependencies = [
        ('appSports', '0005_auto_20191107_1117'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sala',
            name='id',
            field=models.IntegerField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False),
        ),
    ]
