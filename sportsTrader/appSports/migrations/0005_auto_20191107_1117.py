# Generated by Django 2.2.6 on 2019-11-07 14:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('appSports', '0004_sala_password'),
    ]

    operations = [
        migrations.AlterField(
            model_name='sala',
            name='id',
            field=models.IntegerField(auto_created=True, primary_key=True, serialize=False),
        ),
    ]
