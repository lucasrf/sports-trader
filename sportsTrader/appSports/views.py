from django.shortcuts import render,redirect, render_to_response
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_protect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserChangeForm
from django.contrib.auth.models import User
from django.views.generic import CreateView
from django.urls import reverse_lazy
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .models import sala,jogo, campeonato, aposta, resultado
import re
from datetime import datetime

def getJogos():
    jogos_list = jogo.objects.all()
    jogos = []
    for j in jogos_list:
        matchDate = j.date.split('/')
        matchHour = j.hora.split(':')
        if(checkDate(matchDate, matchHour)):
            jogos.append([j.timeA,j.timeB,j.hora,j.date,j.local, j.timeA.id,j.timeB.id,j.camp,j.id])
    return jogos

def checkDate(date,hour):
    currentYear = datetime.now().year
    currentMonth = datetime.now().month
    currentDay = datetime.now().day
    currentHour = datetime.now().hour
    currentMinute = datetime.now().minute
    date = [int(i) for i in date]
    hour = [int(i) for i in hour]
    checker = False

    if(currentYear < date[2]):
        checker = True
    if(currentYear > date[2]):
        checker = False
    if(currentYear == date[2]):
        if(currentMonth < date[1]):
            checker = True
        if(currentMonth > date[1]):
            checker = False
        if(currentMonth == date[1]):
            if(currentDay < date[0]):
                checker = True
            if(currentDay > date[0]):
                checker = False
            if(currentDay == date[0]):
                if(currentHour < hour[0]):
                    checker = True
                if(currentHour > hour[0]):
                    checker = False
                if(currentHour == hour[0]):
                    if(currentMinute < hour[1]):
                        checker = True
                    else:
                        checker = False    
    return checker


def getCampeonatos():
    campeonatos_list = campeonato.objects.all()
    campeonatos = []
    for c in campeonatos_list:
        campeonatos.append(c.nome)
    return campeonatos

def home(request):
    if request.session._session:
        return redirect('/logado/')
    else:
        jogos = getJogos()
        camps = getCampeonatos()
        return render(request,'home.html',{'jogos':jogos, 'camps':camps})

#@login_required(login_url='login/')
def register_apostador(request):
    if request.session._session:
        return redirect('/logado/')
    else:
        return render(request, 'register.html')

def set_apostador(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    password2 = request.POST.get('password2')
    first_name = request.POST.get('first_name')
    last_name = request.POST.get('last_name')
    email = request.POST.get('email')

    passwordIsOk = False
    passwordIsSecure = False
    emailIsOk = False

    if password == password2:
        passwordIsOk = True
    else:
        messages.error(request,'As senhas digitadas são diferentes!')
        return redirect('/register/')
    
    if passwordIsOk and len(password) >= 8:
        pattern_letra = '[a-zA-Z]'
        pattern_numero = '[0-9]'
        nLetra = len(re.findall(pattern_letra,password))
        nNumero = len(re.findall(pattern_numero,password))
        if nLetra > 0 and nNumero > 0:
            passwordIsSecure = True
        else:
            messages.error(request,'A senha precisa ter pelo menos uma letra e um número')
            return redirect('/register/') 
    else:
        messages.error(request,'A senha precisa ter pelo menos 8 caracteres')
        return redirect('/register/')
    
    if email is not None:
        email_pattern = '^[a-zA-Z0-9.!#$%&\'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$'
        if len(re.findall(email_pattern,email)) == 1:
            emailIsOk = True
        else:
            messages.error(request,'E-mail Inválido')
            return redirect('/register/')
    else:
        messages.error(request,'Insira um e-mail')
        return redirect('/register/')

    if emailIsOk:
        try:
            u = User.objects.get(email = email)
            emailIsOk = False
            messages.error(request,'O e-mail inserido já está sendo utilizado!\nTente outra vez!')
            return redirect('/register/')
        except:
            pass
    
    if passwordIsSecure and passwordIsOk and username is not None and first_name is not None and last_name is not None and emailIsOk:
        try:
            user = User.objects.create_user(username = username,email = email,password = password)
            user.last_name = last_name
            user.first_name = first_name
            user.save()
            return redirect('/login/')
        except:
            messages.error(request, 'Usuário '+ username + ' já está sendo utilizado!\nTente outra vez!')
            return redirect('/register/')     
    else:
        messages.error(request,'Por favor, preencha todos os campos!')
        return redirect('/register/')

def loginView(request):
    if request.session._session:
        return redirect('/logado/')
    else:
        return render(request,'login.html')

@csrf_protect   
def login_submit(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username = username, password = password)

        if user is not None:
           login(request, user)
          
           return redirect('/logado/')
        else:
            messages.error(request,'Usuário ou senha invalidos')
    return redirect('/login/')

@login_required(login_url='/login/')
def index(request):
    jogos = getJogos()
    camps = getCampeonatos()
    if request.user.has_perm('appSports.add_sala'):
            moderador = True
    else:
        moderador = False
    return render(request,'index.html',{'jogos':jogos, 'camps':camps, 'moderador':moderador})
    

def indexSubmit(request):
    aposta = []
    jogos = getJogos()
    a = 0
    for j in jogos:
        aposta.append([request.POST.get('a' + str(j[8])),request.POST.get(str(j[8]))])
    for i in range(len(aposta)):
        if aposta[i][0]:
            a = aposta[i][1]

    response = redirect('/apostas/')
    response.set_cookie('aposta',a)
    return response

def logout_user(request):

    logout(request)

    return redirect('/login/')


def cadastrar(request):
    context = {
        "text": "marcio.bict@outlook.com"
    }
    return render(request,'cadastrar.html',context)


def salas(request):
    if request.session._session:
        if request.user.has_perm('appSports.add_sala'):
            return render(request,'salas.html',)
        else:
            return redirect('/salas/verSalas/')
    else:
        messages.error(request, 'É necessário estar logado para acessar às Salas')
        return redirect('/login/')

def criarSala(request):
    if request.session._session:
        return render(request, 'criarSala.html')
    else:
        messages.error(request, 'É necessário estar logado para acessar às Salas')
        return redirect('/login/')

def cadastrarSala(request):
    s = sala()
    try:
        obj = sala.objects.latest('id')
        s.id = int(obj.id) + 1
    except:
        s.id = 1

    nome = request.POST.get("nome")
    s.nome = nome

    qApostador = request.POST.get("qntApostador")
    try:
        s.qtdApostador = int(qApostador)
    except:
        messages.error(request,'Valor inválido de quantidade de Apostadores')
        return redirect('/salas/criarSala/')

    password = request.POST.get('password')
    password2 = request.POST.get('password2')

    if request.POST.get("Privada"):
        privada = True
    else:
        privada = False

    s.salaPrivada = privada

    if privada:
        if password != password2:
            messages.error(request,"As senhas digitadas não são iguais")
            return redirect('/salas/criarSala/')
        else:
            if password == '':
                messages.error(request,"Salas privadas necessitam de uma senha!\nPor favor, insira uma senha!")
                return redirect('/salas/criarSala/')
            else:
                s.password = password
    else:
        s.password = ''
    # s.save()
    try:
        s.save()
        return redirect('/salas/verSalas/')
    except:
        messages.error(request,'Erro ao criar sala!')
        return redirect('/salas/criarSala/')

def verSala(request):
    if request.session._session:
        salas_list = sala.objects.all()
        nomes = []
        for s in salas_list:
            nomes.append([s.nome, s.salaPrivada, s.qtdApostador])

        return render(request,'verSala.html',{'nomes':nomes})
    else:
        messages.error(request, 'É necessário estar logado para acessar às Salas')
        return redirect('/login/')

def apostas(request):
    if request.session._session:
        aposta = request.COOKIES['aposta']
        jogos = getJogos()
        for j in jogos:
            if int(aposta) == int(j[8]):
                time1 = j[0]
                time2 = j[1]
                imgTime1 = '../static/imagem/'+ str(j[5]) +'.png'
                imgTime2 = '../static/imagem/'+ str(j[6]) +'.png'
        
        return render(request,'apostas.html',{'time1':time1,'time2':time2,'imgTime1':imgTime1,'imgTime2':imgTime2,})
    else:
        messages.error(request, 'É necessário estar logado para realizar apostas')
        return redirect('/login/')
#verificar sessão

def apostasSubmit(request):
    apost = aposta()
    timeA = request.POST.get("timeA")
    timeB = request.POST.get("timeB")
    jogoId = request.COOKIES['aposta']
    try:
        apostador = request.user
        apost.golsTimeA = int(timeA)
        apost.golsTimeB = int(timeB)
        apost.partida = jogo.objects.get(id = int(jogoId))
        apost.apostador = apostador
        apost.valor = 10
        apost.save()    
        messages.success(request,"Sua aposta foi feita com sucesso!")
    except:
        messages.error(request,"Sua aposta não pode ser feita! Tente outra vez!")
    return redirect("/logado/")

def showApostas(request):
    if request.session._session:
        aps_list = aposta.objects.all()
        apostador = request.user
        jogos = []
        for a in aps_list:
            if(a.apostador == apostador):
                status = apostaStatus(a.partida,a.golsTimeA,a.golsTimeB)
                jogos.append([a.partida.timeA,a.partida.timeB,a.partida.hora,a.partida.date,a.partida.local, a.partida.timeA.id,a.partida.timeB.id,a.partida.camp,a.partida.id,a.golsTimeA,a.golsTimeB,a.id,status[0],status[1]])
        return render(request,'showApostas.html',{'jogos':jogos})
    else:
        messages.error(request,"É preciso estar logado para ver suas apostas")
        return redirect('/logado/')

def apostaStatus(partida,golsA,golsB):
    res = resultado.objects.all()
    chk = True
    for r in res:
        if r.partida == partida:
            chk = False
    if chk:
        return ['azul','Em Aberto']
    else:
        result = resultado.objects.get(partida = partida)
        if golsA == result.placarA and golsB == result.placarB:
            return ['verde', 'Vencida']
        else:
            return['vermelho','Perdida']

def showApostasSubmit(request):
    aps_list = aposta.objects.all()
    apostador = request.user
    apostas = []
    for a in aps_list:
        if(a.apostador == apostador):
            apostas.append(a.id)
    for iD in apostas:
        if request.POST.get('a'+str(iD)):
            idExcluir = iD
    apostaExcluir = aposta.objects.get(id=idExcluir)
    partida = apostaExcluir.partida
    if(checkDate(partida.date.split('/'),partida.hora.split(':'))):
        aposta.delete(apostaExcluir)
        messages.success(request,'Aposta excluida com sucesso!')
    else:
        messages.success(request,'Aposta não pode ser excluída, porque o jogo já iniciou!')
    return redirect('/showApostas/')

def checkResultado(partida):
    resultados_list = resultado.objects.all()
    for r in resultados_list:
        if(partida.id == r.partida.id):
            return False
    return True

def resultados(request):
    if request.user.has_perm('appSports.add_sala'):
        jogos_list = jogo.objects.all()
        jogos = []
        for j in jogos_list:
            matchDate = j.date.split('/')
            matchHour = j.hora.split(':')
            if(not checkDate(matchDate, matchHour) and checkResultado(j)):
                jogos.append([j.timeA,j.timeB,j.hora,j.date,j.local, j.timeA.id,j.timeB.id,j.camp,j.id])

        return render(request,'resultados.html',{'jogos':jogos})
    else:
        messages.error(request, 'Você não tem permissão para acessar esta área!')
        return redirect('/logado/')

def resultadosSubmit(request):
    jogos_list = jogo.objects.all()
    jogos = []
    result = resultado()
    for j in jogos_list:
        matchDate = j.date.split('/')
        matchHour = j.hora.split(':')
        if(not checkDate(matchDate, matchHour) and checkResultado(j)):
            if request.POST.get('a' + str(j.id)):
                partida = jogo.objects.get(id = j.id)
                try:
                    golsA = int(request.POST.get('timeA'+ str(j.id)))
                    golsB = int(request.POST.get('timeB'+ str(j.id)))
                    result.partida = partida
                    result.placarA = golsA
                    result.placarB = golsB
                    result.save()
                except:
                    messages.error(request,'Número de gols inválido! Tente outra vez!')
    return redirect('/resultados/')
    

# Create your views here.
