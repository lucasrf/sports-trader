from django.db import models
from django import forms
from django.contrib.auth.models import User

class sala(models.Model):
    id = models.IntegerField(primary_key=True)
    nome = models.CharField(max_length=30)
    qtdApostador = models.IntegerField()
    date = models.DateTimeField(auto_now_add=True)
    #user = models.ForeignKey(User,on_delete = models.CASCADE)
    salaPrivada = models.BooleanField(default= False)
    password = models.CharField(max_length=20,default='')

    def __str__(self):

        return str(self.id)

class times(models.Model):
    id = models.IntegerField(primary_key= True)
    nome = models.CharField(max_length=30)

    def __str__(self):
        return str(self.nome)

class campeonato(models.Model):
    id = models.IntegerField(primary_key= True)
    nome = models.CharField(max_length=30)
    
    def __str__(self):
        return str(self.nome)

class jogo(models.Model):
    id = models.AutoField(primary_key=True)
    date = models.CharField(max_length=30)
    timeA = models.ForeignKey(times,on_delete = models.CASCADE, related_name='timeA')
    timeB  = models.ForeignKey(times, on_delete = models.CASCADE, related_name='timeB')
    camp = models.ForeignKey(campeonato, on_delete= models.CASCADE)
    local = models.CharField(max_length=30)
    hora = models.CharField(max_length=30)

    def __str__(self):

        return str(self.id)

class aposta(models.Model):
    id = models.AutoField(primary_key=True)
    apostador = models.ForeignKey(User,on_delete = True)
    partida = models.ForeignKey(jogo,on_delete= True)
    golsTimeA = models.IntegerField()
    golsTimeB = models.IntegerField()
    dataDaAposta = models.DateTimeField(auto_now_add= True)
    valor = models.FloatField()

class resultado(models.Model):
    id = models.AutoField(primary_key=True)
    partida = models.ForeignKey(jogo,on_delete = True)
    placarA = models.IntegerField()
    placarB = models.IntegerField()



    