from django.contrib import admin
from .models import sala,jogo,times, campeonato, aposta


admin.site.register(sala)
admin.site.register(jogo)
admin.site.register(times)
admin.site.register(campeonato)
admin.site.register(aposta)